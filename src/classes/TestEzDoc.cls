@isTest
private class TestEzDoc {
   	@testSetup
    private static void setupTemplateData() {
    	EzDocTemplate__c template = new EzDocTemplate__c();
        template.Append_Timestamp__c = false;
        template.Store_as_Attachment__c = false;
        template.Open_File__c = true;
        template.Static_Resource_Name__c = 'testdoc';
        template.Include_Relations__c = '';
        template.Name = 'EzTest';
        template.Output_Extension__c = 'docx';
        template.Output_Name__c = 'apextestdoc';
        insert template;
    }
    private static testMethod void testEzDocJson() {
        //
        // Fetch template object
        EzDocTemplate__c template = [SELECT Id, Name FROM EzDocTemplate__c WHERE Name='EzTest' LIMIT 1];
        System.assert(template <> null, 'Template setup failure');
        //
        // Use the same template object to test (no child relations)
        Test.startTest();

        Object o = EzDoc.getJson(template.Id, new List <String> { });
        String jsonString = JSON.serialize(o);
        //
        // Cast back and see if it works
        try { 
            EzDocTemplate__c backToSobject = (EzDocTemplate__c)JSON.deserializeStrict(jsonString, EzDocTemplate__c.class);
            System.assertEquals('EzTest', backToSobject.Name, 'Incorrect JSON response');
        }
        catch(Exception e) {
            System.assert(false, 'Error deserializing to EzDocTemplate__c');
        }
        
        System.Debug('### '+o);
        Test.stopTest();
    }
    private static testMethod void testEzDocTpl() {
        //
        // Fetch template object
        EzDocTemplate__c template = [SELECT Id, Name FROM EzDocTemplate__c WHERE Name='EzTest' LIMIT 1];
        System.assert(template <> null, 'Template setup failure');
        //
        // Use the same template object to test       
        Test.startTest();

        Object o = EzDoc.getTpl(template.Id);
        String jsonString = JSON.serialize(o);
        //
        // Cast back and see if it works
        try { 
            EzDocTemplate__c backToSobject = (EzDocTemplate__c)JSON.deserializeStrict(jsonString, EzDocTemplate__c.class);
            System.assertEquals('EzTest', backToSobject.Name, 'Incorrect JSON response');
        }
        catch(Exception e) {
            System.assert(false, 'Error deserializing to EzDocTemplate__c');
        }
        
        System.Debug('### '+o);
        Test.stopTest();
    }
    private static testMethod void testUpload() {
        //
        // Fetch template object
        EzDocTemplate__c template = [SELECT Id, Name FROM EzDocTemplate__c WHERE Name='EzTest' LIMIT 1];
        System.assert(template <> null, 'Template setup failure');
        //
        // Use the same template object to test
        Test.startTest();
        String bodyTxt = 'Ez Test Body';
		String attId = EzDoc.doUploadAttachment(template.Id, bodyTxt, template.Name);
        System.assertEquals('Attachment',EzDocObjectUtil.getSobjectName(attId),'Something went wrong!');
        Test.stopTest();
    }  
    private static testMethod void testErrors() {
        //
        // Fetch template object
        EzDocTemplate__c template = [SELECT Id, Name FROM EzDocTemplate__c WHERE Name='EzTest' LIMIT 1];
        System.assert(template <> null, 'Template setup failure');
        //
        // Use the same template object to test
        Test.startTest();
        String bodyTxt = 'Ez Test Body';
		String attId = EzDoc.doUploadAttachment(null, bodyTxt, template.Name);
        System.assertEquals('Need a record Id!',attId,'Wrong message!!');
        String dummyBody = null;
        attId = EzDoc.doUploadAttachment(template.Id, dummyBody, template.Name);
        System.assertEquals('Attachment Body was null',attId,'Wrong message!!');
        Test.stopTest();
    }   
    private static testMethod void testEzProps() {
        Test.startTest();
		System.assertEquals(null, EzDoc.docTpl.Id, 'Expected null object');
        Test.stopTest();
    }      
}