global with sharing class EzDoc {
    public EzDoc() {} 
    
    @RemoteAction
    @AuraEnabled
    global static Object getJson(String recordId, List<String> relationsToInclude) {
        //
        // Retrieves the data based on the master record id and a list of relation names for related data
        System.Debug('Fetching record as JSON');
        try {
            Set<String> rels = new Set<String>();
            Map<String,String> relFilters = new Map<String,String>();
            for(String s : relationsToInclude) {
                String filterFieldName = null;
                if(s.contains('(')) {
                    //
                    // Filter field defined
					Pattern filterFieldPattern = Pattern.compile('\\((.*)\\)');
                    Matcher myMatcher = filterFieldPattern.matcher(s); 
                    if(myMatcher.find()) {
                        System.Debug('## found '+myMatcher.group(1));   
                        filterFieldName = myMatcher.group(1);
                        s = s.replace('('+filterFieldName+')', '');
                        relFilters.put(s.trim(),filterFieldName);
                    }
                    
                }
                System.Debug('#### s '+s.trim());
                rels.add(s.trim());
            }
            String objectName = EzDocObjectUtil.getSobjectName(recordId);
            String mainFieldClause = EzDocObjectUtil.allFieldsCommaSeparated(objectName);
            Map<String, String> allRels = EzDocObjectUtil.relationToSobject(objectName, rels);
            String query = 'SELECT '+mainFieldClause;
            for(String relName : allRels.keySet()) {
                String childFieldClause = EzDocObjectUtil.allFieldsCommaSeparated(allRels.get(relName));
                String clause = ', (SELECT '+childFieldClause+' FROM '+relName+') ';
                if(relFilters.containsKey(relName)) {
                	clause = ', (SELECT '+childFieldClause+' FROM '+relName+' WHERE '+relFilters.get(relName)+'=true) ';
                } 
                
				query += clause;
            }
            query += ' FROM '+objectName+' WHERE Id = \''+recordId+'\'';
            System.Debug('### query '+query);
            SObject mainRecord = Database.query(query);
            String objectJson = JSON.serialize(mainRecord); 
            return JSON.deserializeUntyped(objectJson);            
        } 
        catch(Exception e) {
            System.Debug('### Exception '+e.getMessage());
            return null;
        }
    }
    
    @RemoteAction
    @AuraEnabled
    global static Object getTpl(String tplId) {
        //
        // Fetches the template settings
		EzDocTemplate__c ezdTpl = [SELECT Id, Name, Static_Resource_Name__c, Output_Name__c,Close_Window_Delay__c,VisualForce_Behavior__c, Output_Extension__c, Append_Timestamp__c,  Store_as_Attachment__c, Include_Relations__c FROM EzDocTemplate__c WHERE Id = :tplId];
        String objectJson = JSON.serialize(ezdTpl); 
        system.debug('### JSON '+objectJson);
        return JSON.deserializeUntyped(objectJson);
    }

    @RemoteAction
    @AuraEnabled
    public static String doUploadAttachment(String recId, String attachmentBody, String attachmentName) {
        System.Debug('### UPLOADING');
        if(recId != null) {
            if(attachmentBody != null) {
	            Attachment att = new Attachment();
                att.Body = EncodingUtil.base64Decode(attachmentBody);
                att.Name = attachmentName;
                att.parentId = recId;
                upsert att;
                System.Debug('### SUCCESS');
                return att.Id;
            } else {
                return 'Attachment Body was null';
            }
        } else {
			return 'Need a record Id!';
        }
    }
    
    //
    // Holds the template based on url param tplId
    public static EzDocTemplate__c docTpl {
        get {
            if(docTpl == null) {
                EzDocTemplate__c[] docTpls = [SELECT Id, Name, Static_Resource_Name__c, Close_Window_Delay__c, VisualForce_Behavior__c, Output_Name__c,	Output_Extension__c,  Append_Timestamp__c, Store_as_Attachment__c, Include_Relations__c FROM EzDocTemplate__c WHERE Id = :ApexPages.currentPage().getParameters().get('tplId')];
                if(docTpls <> null && docTpls.size()==1) {
                    docTpl = docTpls[0];
                } else {
                    docTpl = new EzDocTemplate__c();
                }
            }
            return docTpl;
        }
        set;
    }
}