public class EzDocObjectUtil {
	public static String allFieldsCommaSeparated(String sobjectName) {
        String output = '';
		Set<String> allFields = getFieldList(sobjectName);
        for(String fieldName : allFields) {
            if(fieldName.toLowerCase()=='id') {
                continue; // Adding Id at the end; saves the comma hassle
            }
            output += fieldName+', ';
        }
        output += 'Id';
        return output;        
    }

    //
    // Returns the name of the SObject based on provided Id
    public static String getSobjectName(ID recordId) {
        if(recordId == null) {
        	throw new EzDocObjectUtilException('Illegal id '+recordId);   
        }   
        String keyPrefix = String.valueOf(recordId).substring(0,3);
        if(sobjectKeyPrefixMap.containsKey(keyPrefix)) {
            return sobjectKeyPrefixMap.get(keyPrefix);
        } else {
            throw new EzDocObjectUtilException('Unknown sobject '+keyPrefix);
        }
        return null;
    } 
    //
    // Checks if a field exists on an sobject
    public static Boolean fieldExists(String objectName, String fieldName) {
        Map<String, Schema.SObjectType> allObj = getAllObjects();
        if(allObj.containsKey(objectName)) {
            Schema.SObjectType sot =  allObj.get(objectName);  
            Map<String,Schema.SObjectField> fieldMap = sot.getDescribe().fields.getMap();
            return fieldMap.containsKey(fieldName); 
        } else {
            return false;
        }
    }
    public static Set<String> getFieldList(String objectName) {
        Map<String, Schema.SObjectType> allObj = getAllObjects();
        if(allObj.containsKey(objectName)) {
            Schema.SObjectType sot =  allObj.get(objectName);  
            return sot.getDescribe().fields.getMap().keySet();
        } else {
            return new Set<String>();
        }
    }    
    public static Boolean objectExists(String objectName) {
        return getAllObjects().containsKey(objectName);
    }
    //
    // Map child relation to SObject names
    public static Map<String, String> relationToSobject(String objectName) {
		Map<String, String> relToSobjectMap = new Map<String, String>();
        if(getAllObjects().containsKey(objectName)) {
           	Schema.DescribeSObjectResult sor = getAllObjects().get(objectName).getDescribe();
            List<Schema.ChildRelationship> relations = sor.getChildRelationships();
            for(Schema.ChildRelationship rel : relations) {
                if(rel.getRelationshipName() == null) {
                    continue;
                }
                relToSobjectMap.put(rel.getRelationshipName(), rel.getChildSObject().getDescribe().getName());
            }
        }
        return relToSobjectMap;
    } 
    public static Map<String, String> relationToSobject(String objectName, Set<String> onlyIn) {
		Map<String, String> relToSobjectMap = new Map<String, String>();
        if(getAllObjects().containsKey(objectName)) {
           	Schema.DescribeSObjectResult sor = getAllObjects().get(objectName).getDescribe();
            List<Schema.ChildRelationship> relations = sor.getChildRelationships();
            for(Schema.ChildRelationship rel : relations) {
                if(rel.getRelationshipName() == null) {
                    continue;
                }
                if(onlyIn.contains(rel.getRelationshipName())) {
                	relToSobjectMap.put(rel.getRelationshipName(), rel.getChildSObject().getDescribe().getName());    
                }
            }
        }
        return relToSobjectMap;
    }       
    public static Map<String, Schema.SObjectType> getAllObjects() {
        return Schema.getGlobalDescribe();
    }
    public static Map<String, Schema.SObjectType> getAllCreatableObjects() {
        Map<String, Schema.SObjectType> objMap = Schema.getGlobalDescribe();
        List<String> objNames = new List<String>(objMap.keySet());
        Schema.DescribeSObjectResult[] descRes = Schema.describeSObjects(objNames);
        Map<String, Schema.SObjectType> output = new Map<String, Schema.SObjectType>();
        for(Schema.DescribeSObjectResult objDesc : descRes) {
            if(objDesc.isCreateable() && objDesc.isAccessible()) {
                output.put(objDesc.getName(), objDesc.getSobjectType());
            }
        }
        return output;
    }    
    public static SObject getSobInstance(String objectName) {
        Map<String, Schema.SObjectType> allObj = getAllObjects();
        if(allObj.containsKey(objectName)) {
        	Schema.SObjectType sobType = allObj.get(objectName);    
            return sobType.newSObject();
        } else {
            return null;
        }   
    }
    //
    // Maps Sobjects to keyprefixes
    public static Map<String, String> sobjectKeyPrefixMap {
        get {
            if(sobjectKeyPrefixMap == null) {
                sobjectKeyPrefixMap = new Map<String, String>();
                Map<String, Schema.SObjectType> allObjects = getAllObjects();
                List<String> typeNames = New List<String>(allObjects.keySet());
                List<Schema.DescribeSObjectResult> typeList = Schema.describeSObjects(typeNames);
                for(Schema.DescribeSObjectResult res : typeList) {
                	sobjectKeyPrefixMap.put(res.getKeyPrefix(),res.getName());    
                }
           
            }
            return sobjectKeyPrefixMap;
        }
        set;
    }
    //
    // Custom exception class
    public class EzDocObjectUtilException extends Exception {
        
    }
}