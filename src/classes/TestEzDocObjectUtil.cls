@isTest
private class TestEzDocObjectUtil {
    private static testMethod void testObjectUtil() {
        Test.startTest();
        System.assert(!String.isEmpty(EzDocObjectUtil.allFieldsCommaSeparated('User')), 'No User object, or other serious error!');
		System.assert(EzDocObjectUtil.fieldExists('User','UserName'), 'No User object, or other serious error!');
        System.assertEquals('User',EzDocObjectUtil.getSobjectName(System.UserInfo.getUserId()), 'No User object, or other serious error!');
        System.assert(EzDocObjectUtil.getFieldList('User').size()>0, 'No User object, or other serious error!');
        System.assert(EzDocObjectUtil.objectExists('User'), 'No User object, or other serious error!');
        System.assert(EzDocObjectUtil.relationToSobject('User').size()>0, 'No User object, or other serious error!');
        System.assert(EzDocObjectUtil.relationToSobject('Account', new Set<String> { 'Contacts' } ).size()>0, 'No Account/Contact object!');
        System.assert(EzDocObjectUtil.getAllCreatableObjects().size()>0, 'No objects!!!');
        System.assertEquals(null, EzDocObjectUtil.getSobInstance('User').Id, 'Huh? And id?');
        Test.stopTest();
    }
     private static testMethod void testObjectUtilExceptions() {
        Test.startTest();
       	try {
            EzDocObjectUtil.getSobjectName(null);
		}
        catch(Exception e) {
            system.assert(true);
        }
        try {
            EzDocObjectUtil.getSobjectName('nonsenseid');
		}
        catch(Exception e) {
            system.assert(true);
        }
        try {
            EzDocObjectUtil.getFieldList('blaaahblah');
        }
        catch(Exception e) {
            system.assert(true);
        }
     }   
}